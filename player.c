#include "bot.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double mex;
double mey;

int cmpfunc (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b);
}

int distanceFood(const void *a, const void *b)
{
    double ax = ((struct food*)a)->x;
    double ay = ((struct food*)a)->y;
    double bx = ((struct food*)b)->x;
    double by = ((struct food*)b)->y;
    
    
    //Distance from me to a
    double dista = sqrt((mex-ax)*(mex-ax) + (mey-ay)*(mey-ay));
    
    //Distance from me to b
    double distb = sqrt((mex-bx)*(mex-bx) + (mey-by)*(mey-by));
    
    //sub dista - distb
    return dista - distb;
}

int distancePlayer(const void *a, const void *b)
{
    double ax = ((struct player*)a)->x;
    double ay = ((struct player*)a)->y;
    double bx = ((struct player*)b)->x;
    double by = ((struct player*)b)->y;
    
    
    //Distance from me to a
    double dista = sqrt((mex-ax)*(mex-ax) + (mey-ay)*(mey-ay));
    
    //Distance from me to b
    double distb = sqrt((mex-bx)*(mex-bx) + (mey-by)*(mey-by));
    
    //sub dista - distb
    return dista - distb;
}

int distanceVirus(const void *a, const void *b)
{
    double ax = ((struct cell*)a)->x;
    double ay = ((struct cell*)a)->y;
    double bx = ((struct cell*)b)->x;
    double by = ((struct cell*)b)->y;
    
    
    //Distance from me to a
    double dista = sqrt((mex-ax)*(mex-ax) + (mey-ay)*(mey-ay));
    
    //Distance from me to b
    double distb = sqrt((mex-bx)*(mex-bx) + (mey-by)*(mey-by));
    
    //sub dista - distb
    return dista - distb;
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    mex = me.x;
    mey = me.y;
    
    qsort(foods,nfood, sizeof(struct food), distanceFood);
    qsort(players, nplayers, sizeof(struct player), distancePlayer);
    qsort(virii, nvirii, sizeof(struct cell), distanceVirus);
    
    if(me.totalMass < 1000) //
    {
        act.dx = (foods[0].x - me.x) * 100;
        act.dy = (foods[0].y - me.y) * 100;
        act.split = 0;
        act.fire = 0;
    }
    
    else if(me.totalMass >= 500) //go towards player and split when I am good size
    {
        act.dx = (players[0].x - me.x) * 100;
        act.dy = (players[0].y - me.y) * 100;
        act.split = 1;
        act.fire = 0;
    }
    
    else if(me.totalMass > 100) //avoid virus?
    {
        act.dx = (virii[0].x + me.x) * 100;
        act.dy = (virii[0].y + me.y) * 100;
        act.split = 0;
        act.fire = 0;
    }
    
    //double closefdist = 5000;
    //int first;
    
    
    
    
    /*/supposed to find some food lol
    for (int i = 0; i <= sizeof(foods); i++)
    {
        double c1x = foods[i].x;
        double c1y = foods[i].y;
        double c2x = foods[i+1].x;
        double c2y = foods[i+1].y;
        
        double dist1 = sqrt((c1x - mex)*(c1x-mex) + (c1x-mey)*(c1x-mey));
        double dist2 = sqrt((c2x - mex)*(c2x-mex) + (c2x-mey)*(c2x-mey));
        
        if(dist1 < dist2 && dist1 < closefdist)
        {
            closefdist = dist1;
            first = i;
        }
        else if(dist2 < dist1 && dist2 < closefdist)
        {
            closefdist = dist2;
            first = i+1;
        }
        
    }*/
    
    
    return act;
}

